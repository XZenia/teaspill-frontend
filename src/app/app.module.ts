import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router'

import { AppComponent } from './app.component';
import { ChatInboxComponent } from './components/chat-inbox/chat-inbox.component';
import { HttpClientModule } from '@angular/common/http';
import { RoomListComponent } from './components/room-list/room-list.component';
import { RoomComponent } from './components/room/room.component';
import { HeaderComponent } from './components/header/header.component';
import { CreateRoomComponent } from './components/create-room/create-room.component';
import { RoomListItemComponent } from './components/room-list/room-list-item/room-list-item.component';
import { AlertComponent } from './components/alert/alert.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { RegisterPageComponent } from './components/register-page/register-page.component';

const routes: Routes = [
  { path: 'create-room', component: CreateRoomComponent },
  { path: 'rooms', component: RoomListComponent },
  { path: 'room/:id', component: RoomComponent },
  { path: 'join', component: RegisterPageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: '**', component: RoomListComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    ChatInboxComponent,
    RoomListComponent,
    RoomComponent,
    HeaderComponent,
    CreateRoomComponent,
    RoomListItemComponent,
    LoginPageComponent,
    AlertComponent,
    RegisterPageComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports:[RouterModule]
})
export class AppModule { }
