export class Message {
    username: string;
    message: string;
    roomId: string;
    date: Date;

    constructor(username: string, message: string, roomId: string, date: Date) { 
        this.username = username;
        this.message = message;
        this.roomId = roomId;
        this.date = date ?? new Date();
    }
}
