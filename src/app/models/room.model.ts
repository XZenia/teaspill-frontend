export class Room {
    id: string;
    name: string;
    created_at: Date;
    updated_at: Date;

    constructor(roomId: string = "", name: string = "", created_at: Date = new Date(), updated_at: Date = new Date()) { 
        this.id = roomId;
        this.name = name;
        this.created_at = created_at ?? new Date();
        this.updated_at = updated_at ?? new Date();
    }
}
