import { Injectable } from '@angular/core';
import { HttpClient,HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  constructor(private http: HttpClient) { }

  getRoomMessages(id: string) {
    const params = new HttpParams().set('roomId', id);
    return this.http.get(environment.roomMessages + id, {params});
  }

  getRoomList(){
    return this.http.get(environment.roomList);
  }

  getRoomData(id: string){
    const params = new HttpParams().set('id', id);
    return this.http.get(environment.roomInfo + id, {params});
  }
}
