import { Component, OnInit } from '@angular/core';
import { RoomsService } from '../../services/rooms.service';
import { HttpClient } from '@angular/common/http';
import {Router} from "@angular/router"
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-room',
  templateUrl: './create-room.component.html',
  styleUrls: ['./create-room.component.css']
})

export class CreateRoomComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router, private roomsService: RoomsService) { }

  ngOnInit(): void {
  }

  AddRoom(roomName: HTMLInputElement) {
    if (roomName.value !== ""){
      this.http.post<any>(environment.createRoomUrl, { roomName: roomName.value })
      .subscribe(data => {
        if (data['code'] == 200) {
          this.router.navigate(['/room/:id', {id: data.room.id}]);
        } else {
          this.router.navigate(['/rooms']);
        }
      });
    }
  }
}
