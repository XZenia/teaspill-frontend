import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {

  roomId: string = "";

  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(params => this.roomId = params['id'])
  }

  ngOnInit(): void {

  }

}
