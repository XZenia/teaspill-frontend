import { Component, OnInit } from '@angular/core';
import { RoomsService } from '../../services/rooms.service';

@Component({
  selector: 'room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {

  rooms: any = [];

  constructor(private roomsService: RoomsService) { }

  ngOnInit(): void {
    this.roomsService.getRoomList()
    .subscribe(data => {
      for (const d of (data as any)) {
        this.rooms.push(d);
      }
    });
  }

}
