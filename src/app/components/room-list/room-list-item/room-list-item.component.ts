import { Component, OnInit, Input } from '@angular/core';
import { Room } from '../../../models/room.model';

@Component({
  selector: 'room-list-item',
  templateUrl: './room-list-item.component.html',
  styleUrls: ['./room-list-item.component.css']
})
export class RoomListItemComponent implements OnInit {
  @Input() room: Room;

  constructor() {
    this.room = new Room();
  }

  ngOnInit(): void {
  }

}
