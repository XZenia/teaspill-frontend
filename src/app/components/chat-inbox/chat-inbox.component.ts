import { Component, OnInit, Input } from '@angular/core';
import io from 'socket.io-client';
import { Message } from '../../models/message.model';
import { Room } from '../../models/room.model';
import { RoomsService } from '../../services/rooms.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'chat-inbox',
  templateUrl: './chat-inbox.component.html',
  styleUrls: ['./chat-inbox.component.css']
})

export class ChatInboxComponent implements OnInit {
  message: string = "";
  link: String = "";
  roomMessages: any = [];

  @Input() roomId: string;
  room: Room;

  socket: any;
  otherUser: any;
  peer: RTCPeerConnection;

  currentDataChannel: RTCDataChannel;
  currentUser: any;

  constructor(private roomsService: RoomsService, private messageService: RoomsService) {
    this.roomId = "";
    this.room = new Room();

    this.peer = new RTCPeerConnection;

    this.socket = io(environment.SOCKET_ENDPOINT);
  }

  ngOnInit(): void {
    this.subscribeMessageData();

    this.socket = io(environment.SOCKET_ENDPOINT);
    this.socket.emit("join room", this.roomId);

    this.socket.on('current user', (userId) => {
        this.currentUser = userId;
        console.log(userId);
    });
    
    this.socket.on('other user', (userId) => {
        this.callUser(userId);
        this.otherUser = userId;
    });

    this.socket.on('user joined', (userId) => {
        this.otherUser = userId;
    });

    this.socket.on("offer", (data: any) => this.handleOffer(data));
    this.socket.on("answer", (data: any) =>  this.handleAnswer(data));
    this.socket.on("ice-candidate", (data: any) => this.handleNewICECandidateMsg(data));
  }

  subscribeMessageData(){
    this.messageService.getRoomMessages(this.roomId)
    .subscribe(data => {
      if (data){
        for (const d of (data as any)) {
          this.roomMessages.push(d);
          this.showMessage(d)
        }
      }
    });
  }

  getRoomInformation(){
    if (this.roomId) {
      this.roomsService.getRoomData(this.roomId)
      .subscribe(data => {
        var roomData = Object.entries(data);
        this.room.id = roomData[2][1];
        this.room.name = roomData[1][1];
        if (roomData[3][1]){
          this.room.created_at = roomData[3][1];
        } else {
          this.room.created_at = new Date();
        }

        if (roomData[4][1]){
          this.room.created_at = roomData[4][1];
        } else {
          this.room.created_at = new Date();
        }
      });
    }
  }

  SendMessage(){
    var newMessage = new Message(this.currentUser, this.message, this.roomId, new Date());
    console.log(this.currentUser);
    if (this.message !== ''){

      if (this.currentDataChannel && this.currentDataChannel.readyState == "open"){
        this.currentDataChannel.send(JSON.stringify(newMessage));
      } else {
        this.socket.emit('send-message', JSON.stringify(newMessage));
      }
      
      const element = document.createElement('li');
      element.innerHTML += this.message;
      element.style.background = 'white';
      element.style.padding =  '15px 30px';
      element.style.margin = '10px';
      element.style.textAlign = 'right';
      document.getElementById('message-list')!.appendChild(element);
      this.message = '';
    }
  }

  showMessage(messageData : any){
    const element = document.createElement('li');
    console.log(messageData)
    element.innerHTML = "<strong>"+ messageData.username +"</strong><br/>";
    element.innerHTML += messageData.message;
    element.style.background = 'white';
    element.style.padding =  '15px 30px';
    element.style.margin = '10px';
    document.getElementById('message-list')!.appendChild(element);
  }

  //WebRTC functions
  createPeer(userId: string) {
    const peer = new RTCPeerConnection({
        iceServers: [
            {
                urls: "stun:stun.stunprotocol.org"
            },
            {
                urls: 'turn:numb.viagenie.ca',
                credential: 'muazkh',
                username: 'webrtc@live.com'
            },
        ],
    });

    peer.onicecandidate = (e) => this.handleICECandidateEvent(e);
    peer.onnegotiationneeded = () => this.handleNegotiationNeededEvent(userId);

    return peer;
  }

  callUser(userId: string){
    this.peer = this.createPeer(userId);
    this.currentDataChannel =  this.peer.createDataChannel(this.roomId);
    this.currentDataChannel.onmessage = (e) => this.handleReceiveMessage(e);
  }

  handleOffer(incoming) {
    this.peer =  this.createPeer(incoming.target);
    this.peer.ondatachannel = (event) =>{
        this.currentDataChannel = event.channel;
        this.currentDataChannel.onmessage = (e) => this.handleReceiveMessage(e);
    };
    const desc = new RTCSessionDescription(incoming.sdp);

    this.peer.setRemoteDescription(desc).then(() => {
      console.log("Remote Description Set!");
    }).then(() => {
        return this.peer.createAnswer();
    }).then(answer => {
        return this.peer.setLocalDescription(answer);
    }).then(() => {
        const payload = {
            target: incoming.caller,
            caller: this.socket.id,
            sdp: this.peer.localDescription
        }
        console.log(payload);
        this.socket.emit("answer", payload, this.roomId);
    })
  }

  handleReceiveMessage(e){
    var messageData = JSON.parse(e.data);
    if (messageData) {
      this.showMessage(messageData);
    }
  }

  handleNegotiationNeededEvent(userId: string){
    this.peer.createOffer().then(offer => {
      return this.peer.setLocalDescription(offer);
    }).then(() => {
      const payload = {
        target: userId,
        caller: this.socket.id,
        sdp: this.peer.localDescription
      }

      this.socket.emit("offer", payload, this.roomId);
    }).catch(e => console.log(e));
  }

  handleICECandidateEvent(e) {
    if (e.candidate) {
      const payload = {
          target: this.otherUser,
          candidate: e.candidate,
      }
      this.socket.emit("ice-candidate", payload, this.roomId);
    }
  }

  handleNewICECandidateMsg(incoming) {
    const candidate = new RTCIceCandidate(incoming);
    this.peer.addIceCandidate(candidate)
        .catch(e => console.log(e));
  }

  handleAnswer(message: any) {
    const desc = new RTCSessionDescription(message.sdp);
    this.peer.setRemoteDescription(desc).catch(e => console.log(e));
  }
}
