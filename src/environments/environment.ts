// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  SOCKET_ENDPOINT: 'localhost:3000',
  baseUrl: 'http://localhost:3000/',
  roomMessages: 'http://localhost:3000/room/messages/',
  roomInfo: 'http://localhost:3000/room/',
  roomList: 'http://localhost:3000/rooms',
  createRoomUrl: 'http://localhost:3000/create-room'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
